import java.util.*

class Pharmacy(
    _name: String,
    _street: String,
    _zip: String,
    _town: String,
    _startDate: Date,
    _endDate: Date,
    _callNumber: String
){
    val name : String = _name
    val street : String = _street
    val zip : String = _zip
    val town : String = _town
    val startDate : Date = _startDate
    val endDate : Date = _endDate
    val callNumber : String = _callNumber
}