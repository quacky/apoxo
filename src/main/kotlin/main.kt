import api.XiboCsvColumn
import java.io.File
import java.io.FileInputStream
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.jar.Manifest
import java.util.Properties


fun main() {

    readAndPrintApplicationData()
    val properties = Properties(fileName = System.getProperty("user.dir") + "/apoxo.properties")
    properties.verifyMandatorySettings()
    println()
    if (properties.downloadCSV) {
        downloadCSV(zipCode = properties.zipCode, fromDate = properties.fromDate, toDate = properties.toDate)
    } else {
        println("Do not download csv. Use ${properties.keyDownladCSVFlag}=YES in your property file to change this.")
    }

    // connect to xibo server
    if (properties.uploadCSV) {
        val xiboClientApi = api.XiboClient(auth = properties.getAuthorizationData(), serverURL = properties.serverURL)
        // only test: say hello to xibo
        // val clock: String = xiboClientApi.getClock()
        // println(clock)

        // another test
        // xiboClientApi.createLayout("kotlinLayout")
        val csvFileComma = "pharmaciesComma.csv"

        var xiboDataSetColumns: List<XiboCsvColumn> = xiboClientApi.getColumnID(properties.datasetID)
        xiboDataSetColumns =
            mapCSVColumnsWithXiboColumns(file = File(csvFileComma), xiboColumns = xiboDataSetColumns)
        xiboClientApi.sendCSVToXibo(
            fileName = csvFileComma,
            columnIDs = xiboDataSetColumns,
            datasetID = properties.datasetID
        )
    } else {
        println("Do not upload csv. Use ${properties.keyXiboUploadFlag}=YES in your property file to change this.")
    }
    println("Ciao Kakao.")
}

fun mapCSVColumnsWithXiboColumns(
    file: File,
    xiboColumns: List<XiboCsvColumn>
): List<XiboCsvColumn> {
    val csvFirstLine = file.useLines { it.firstOrNull() }
    println("Match csv columns id with Xibo column ID's")
    val csvColumns = csvFirstLine?.split(",")
    for (xiboColumn in xiboColumns) {
        csvColumns?.forEachIndexed { index, column ->
            if (column == xiboColumn.heading) {
                println("\tMachted Xibo column ${xiboColumn.heading} with csv column $index")
                xiboColumn.csvColumnId = index.toString()
            }
        }
        if (xiboColumn.csvColumnId.isNullOrEmpty()) {
            println("\tWARNING: Xibo column ${xiboColumn.heading} did not match to any csv column.")
        }
    }
    return xiboColumns
}

fun downloadCSV(zipCode: String, fromDate: String, toDate: String): String {
    println("Start downloading CSV file...")
    // user input

    var fromDateQuery = fromDate
    var toDateQuery = toDate
    var zipCodeQuery = "/code/$zipCode"
    if (fromDateQuery.isBlank() or toDateQuery.isBlank()) {
        println("Properties 'fromDate' or 'toDate' are not specified. Use current date for csv download:")
        fromDateQuery = getCurrentDay()
        toDateQuery = fromDateQuery
    }
    if (zipCode.isBlank()) {
        println("No zipCode given. Therefore, do not filter for zip code.")
        zipCodeQuery = ""
    }

    val fileURL =
        "https://apothekennotdienste-saarland.de/calendar/csv/from/$fromDateQuery/to/$toDateQuery$zipCodeQuery"
    val fileDownloader = FileDownloader(_url = fileURL)
    val responseString:String = fileDownloader.getFileAsString()

    // convert delimiter, xibo only understands csv with comma
    val responseStringComma:String = responseString.replace(";", ",")

    // delete empty lines (mostly the last one)
    val regex = Regex(pattern = "(\\r\\n)+")
    val responseStringFormatted= regex.replace(responseStringComma, "")

    val csvFile = "pharmacies.csv"
    val csvFileComma = "pharmaciesComma.csv"

    print("Create csv files...")
    File(csvFile).writeText(responseString)
    File(csvFileComma).writeText(responseStringFormatted)
    return csvFileComma
}

fun getCurrentDay(): String {
    val currentDate = LocalDateTime.now()
    println("current date is: $currentDate")

    val formatter = DateTimeFormatter.ofPattern("yyyyMMdd")
    val formatted = currentDate.format(formatter)
    println("\tformatted date: $formatted")
    return formatted
}

/**
 * Function reads the manifest file and prints
 * application name, version and built time stamp to stdout
 */
fun readAndPrintApplicationData() {
    val manifestResource = Pharmacy::class.java.getResource("/META-INF/MANIFEST.MF")
    val manifest = Manifest()
    manifest.read(manifestResource.openStream())
    val version = manifest.mainAttributes.getValue("Implementation-Version")
    val buildDate = manifest.mainAttributes.getValue("Built-Date")
    val applicationName = manifest.mainAttributes.getValue("Implementation-Title")
    println("\nStart $applicationName $version ($buildDate)\n")
}

