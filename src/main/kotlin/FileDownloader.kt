import okhttp3.OkHttpClient
import okhttp3.Request
import java.io.IOException

class FileDownloader(_url: String) {
    val url:String = _url
    val client = OkHttpClient()
    val request = Request.Builder()
        .url(url)
        .build()

    fun getFileAsString(): String {
        println("Get content of $url")

        client.newCall(request).execute().use { response ->
            if (!response.isSuccessful) throw IOException("Unexpected code $response")
            for ((name, value) in response.headers) {
                println("$name: $value")
            }
            val returnValue = response.body!!.string()
            println(returnValue)
            return returnValue
        }
    }



}