import api.AuthorizationBody
import java.io.File
import java.io.FileInputStream
import java.util.Properties
import kotlin.system.exitProcess

class Properties (fileName:String) {
    val properties:Properties = Properties()
    val propertiesFile = fileName
    val inputStream = FileInputStream(propertiesFile)
    val keyClientID = "xibo.ClientID"
    val keyClientSecret = "xibo.clientSecret"
    val keyServerURL = "xibo.baseURL"
    val keyDataSet = "xibo.datasetID"
    val keyXiboUploadFlag = "xibo.UploadCSV"
    val keyDownladCSVFlag = "downloadCSV"

    init {
        val file = File(fileName)
        if (file.exists()) {
            println("Read configuration file: $fileName")
        } else {
            println("Couldn't find configuratio file for Apoxo: $fileName")
            exitProcess(1)
        }
        properties.load(inputStream)
        properties.forEach { (k, v) -> println("key = $k, value $v") }
        val xiboClientID = properties.getProperty(keyClientID)
    }

    // must happen after init block:
    var downloadCSV:Boolean = properties.getProperty(keyDownladCSVFlag)?.equals("YES") == true
    var uploadCSV:Boolean = properties.getProperty(keyXiboUploadFlag)?.equals("YES") == true
    val zipCode = properties.getProperty("zipCode") ?: ""
    val fromDate:String = properties.getProperty("fromDate") ?: ""
    val toDate:String = properties.getProperty("toDate") ?: ""
    val clientID:String = properties.getProperty(keyClientID) ?: ""
    val clientSecret:String = properties.getProperty(keyClientSecret) ?: ""
    val serverURL:String = properties.getProperty(keyServerURL) ?: ""
    val datasetID:String = properties.getProperty(keyDataSet) ?: ""


    fun getAuthorizationData():AuthorizationBody {
       return AuthorizationBody(clientID=clientID, clientSecret=clientSecret)
    }

    fun verifyMandatorySettings():Boolean {
        var returnValue = true
        if (uploadCSV) {
            if (clientID.isEmpty()) {
                println("Please specifiy $keyClientID in your property file $propertiesFile")
                returnValue = false
            }
            if (clientSecret.isEmpty()) {
                println("Please specifiy $keyClientSecret in your property file $propertiesFile")
                returnValue = false
            }
            if (serverURL.isEmpty()) {
                println("Please specifiy $keyServerURL in your property file $propertiesFile")
                returnValue = false
            }
            if (datasetID.isEmpty()) {
                println("Please specifiy $keyDataSet in your property file $propertiesFile")
                returnValue = false
            }
        }
        if (!returnValue) {
            println("Configuration is incomplete. Abort.")
            exitProcess(1)
        }
        return returnValue
    }

}