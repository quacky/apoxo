package api

import com.google.gson.annotations.SerializedName

data class XiboCsvColumn (
    // xibo fields
    var dataSetColumnId: Int,
    var dataSetId: Int,
    var heading: String,
    var dataTypeId: Int,
    var dataSetColumnTypeId: Int,
    var listContent: String,
    var columnOrder: String,
    var formula: String,
    var dataType: String,
    var remoteField: String,
    var showFilter: Int,
    var showSort: Int,
    var dataSetColumnType: String,

    // own fields
    var csvColumnId: String?,
)