package api

import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File
import kotlin.system.exitProcess

class XiboDataset(private val csvFile: String, val xiboColumnID: List<XiboCsvColumn>) {
    private val overwrite: String = "1"
    private val ignoreFirstRow: String = "1"

    init {
        var file = File(csvFile)
        if (!file.exists()) {
            println("CSV file " + file.absolutePath + " does not exist. Abort")
            exitProcess(1)
        }
    }

    fun getBody(): RequestBody {
        val requestFile = File(csvFile).asRequestBody("text/plain".toMediaTypeOrNull())
        val bodyBuilder = MultipartBody.Builder().setType(MultipartBody.FORM)
            .addFormDataPart(
                "files", csvFile,
                requestFile
            )
            .addFormDataPart("overwrite", "1")
            .addFormDataPart("ignorefirstrow", "1")
        println("Generate column mapping for REST request:")
        for (column in xiboColumnID) {
            if (column.csvColumnId.isNullOrEmpty()) continue
            val parameterName = "csvImport_${column.dataSetColumnId}"
            val csvColumnCorrected = column.csvColumnId.let {
                (it?.toInt()?.plus(1)).toString()
            }
            println("\t${column.heading}: $parameterName := $csvColumnCorrected")
            bodyBuilder.addFormDataPart(name = parameterName, value = csvColumnCorrected)
        }
        return bodyBuilder.build()
    }
}