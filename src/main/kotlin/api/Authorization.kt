package api

import com.google.gson.annotations.SerializedName
import okhttp3.MultipartBody
import okhttp3.RequestBody

interface ServerResponse  {
}
data class AuthorizationResponse (
    @SerializedName("access_token")
    val accessToken:String="",
    @SerializedName("token_type")
    val tokenType:String="",
    @SerializedName("expires_in")
    val expiresIn:String=""
):ServerResponse

class AuthorizationBody(val clientID:String, val clientSecret: String)  {
    private val grantType:String = "client_credentials"

    fun getBody():RequestBody{
        return MultipartBody.Builder()
            .setType(MultipartBody.FORM)
            .addFormDataPart("grant_type", grantType)
            .addFormDataPart("client_id", clientID)
            .addFormDataPart("client_secret", clientSecret)
            .build()
    }
}
