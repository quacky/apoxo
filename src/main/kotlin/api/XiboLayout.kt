package api

import okhttp3.MultipartBody
import okhttp3.RequestBody

class XiboLayout(val name: String) {
    fun getBody(): RequestBody {
        return MultipartBody.Builder()
            .setType(MultipartBody.FORM)
            .addFormDataPart("name", name)
            .build()
    }
}