package api

import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import kotlin.system.exitProcess

class XiboClient (auth:AuthorizationBody, serverURL:String) {
    private val xiboApi: XiboApi
    private val authorizationToken: AuthorizationResponse
    private var authorizationData = auth

    init {
        val retrofit: Retrofit = Retrofit.Builder()
            .baseUrl(serverURL)
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        // create a retrofit instance (at runtime) based on the specified FlickrApi interface
        xiboApi = retrofit.create(XiboApi::class.java)
        authorizationToken = authorize()
    }

    fun getClock(): String {
        val clockRequest: Call<String> = xiboApi.getClock()
        val clockResponse = clockRequest.execute()
        return clockResponse.body().toString()
    }

    private fun authorize(): AuthorizationResponse {
        println("Start authorization...")
        val requestBody: RequestBody = authorizationData.getBody()
        val request =
            xiboApi.authorize(body = requestBody)

        val response = request.execute()
        if (checkResponse(response = response)) {
            println("\t Received authorization token")
            return response.body()!!
        } else {
            println("failed :( ")
            exitProcess(1)
        }
    }

    fun sendCSVToXibo(fileName: String, columnIDs:List<XiboCsvColumn>, datasetID:String) {
        val fileRequest = xiboApi.importDataset(
            body = XiboDataset(csvFile = fileName, xiboColumnID = columnIDs).getBody(),
            token = "${authorizationToken.tokenType} ${authorizationToken.accessToken}",
            datasetID = datasetID
        )
        println("Send file to Mr. Xibo")
        val fileResponse = fileRequest.execute()
        println(fileResponse.body().toString())
        if (fileResponse.isSuccessful && fileResponse.raw().code == 200) {
            println("File hopefully uploaded.")
        } else {
            println("Something went wrong. Abort")
            exitProcess(1)
        }
    }

    fun createLayout(name: String) {
        print("Create Layout: $name \n")
        val layoutRequest = xiboApi.createLayout(
            XiboLayout(name = "KotlinLayout2").getBody(),
            "${authorizationToken.tokenType} ${authorizationToken.accessToken}"
        )
        val layoutResponse = layoutRequest.execute()
        if (checkResponse(response = layoutResponse)) {
            print("Layout created")
            print(layoutResponse.body().toString())
        }
    }

    fun getColumnID(datasetID: String): List<XiboCsvColumn> {
        println("Determine column ID's")
        val request = xiboApi.getColumns(
            datasetID = datasetID,
            token = "${authorizationToken.tokenType} ${authorizationToken.accessToken}"
        )
        val response = request.execute()
        println(response.body().toString())
        if (response.isSuccessful) {
            if (response.body() != null) {
                return response.body()!!
            }
        }
        println("Determining column id failed. Abort.")
        exitProcess(1)
    }

    fun <T : ServerResponse?> checkResponse(response: Response<T>): Boolean {
        println(response.toString())
        if (!response.isSuccessful) {
            println("Some Connection Error happened.")
            println("Response Body: ${response.body().toString()}")
            println("Error Body: ${response.errorBody()?.string()}")
            return false
        }
        return true
    }
}