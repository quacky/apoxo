package api

import com.google.gson.annotations.SerializedName

data class XiboResponse(
        var grid:Boolean,
        var success:Boolean,
        @SerializedName("status")
        var status:Int,
        var message:String,
        var id:String
): ServerResponse
