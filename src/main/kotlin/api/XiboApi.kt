package api

import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

public interface XiboApi {
    @GET("/clock")
    fun getClock(): Call<String>

    @GET("/api/dataset/{dataset_id}/column")
    fun getColumns(
        @Path(value = "dataset_id", encoded = true) datasetID:String,
        @Header("Authorization") token: String
    ): Call<List<XiboCsvColumn>>

    @POST("/api/authorize/access_token")
    fun authorize(
        @Body body: RequestBody
    ): Call<AuthorizationResponse>

    @POST("/api/layout?envelope=1")
    fun createLayout(
        @Body layoutBody: RequestBody,
        @Header("Authorization") token: String
    ): Call<XiboResponse>

    @POST("/api/dataset/import/{dataset_id}")
    fun importDataset(
        @Path(value = "dataset_id", encoded = true) datasetID:String,
        @Body body: RequestBody,
        @Header("Authorization") token: String
    ): Call<String>
}

