# ApoXo

[[_TOC_]]

## Description
**Apo**theke meets **X**ib**o**.<br>
**ApoXo** is a small tool which does the following two jobs:

1) Download the "Dienstbereitschaftssplan der Apotheken im Saarland" as a csv sheet 
   provided by https://apothekennotdienste-saarland.de. The sheet can be filterd by date and zip codes.
   
2) Upload a (the same) csv sheet to a xibo server. 

It's also possible to use only one of the two features. (See configuration options below)

## Requirements
* java version 15.0.1
* optional: xibo server (tested with 2.3.8)
## Usage

ApoXo is configured via the configuration file 'apoxo.properties' which should be placed next 
(in the same folder) to the **ApoXo** application. <br>
See [Configurations below](##Configurations) how to configure the tool.<br>
See [apoxo.properties](apoxo.properties) as an example property file. 
<br>Execute:
```aidl
./Apoxo
```
to run the tool.

## Configurations

### Download pharmacy plan as csv sheet 
**ApoXo** will only download the pharmacy data as csv if you enable the feature with
```aidl
downloadCSV=YES
```
in your 'apoxo.properties' file.
If you do not wish to download any data, just skip this line in your configuration. 


#### Filter by zip code
Example:
```
zipCode=66271,66636
```
Only pharmacies with the given zip codes are downloaded. 
Skip this line to disable the zip code filter.

#### Filter by date
Example: 
```
fromDate=20210101
toDate=20210110
```
Specify dates in the format yyyymmdd which are used as filter.
If not specified, the current day is used.

### Xibo server configuration
**ApoXo** will only upload data to xibo if you enable the feature with 
```
xibo.UploadCSV=YES
```
If you do not wish to upload any data, just skip this line in your 'apoxo.properties' file and 
**ApoXo** will not communicate with xibo. 
All further 'xibo.*' properties does not have any impact in that case.

#### Specify the URL to the xibo server
```
xibo.baseURL=http:/localhost
```
The base url is the main url of your xibo server. If you go to your xibo dashboard (Übersicht), 
the url may look as following: *http://localhost/statusdashboard*

In that case, the baseURL=*http://localhost*

#### Grant access to xibo and determine ClientID and Client Secret
```aidl
xibo.ClientID=abcd
xibo.clientSecret=12345
```
To grant *ApoXo* access to your xibo server, you have to create a new application 
in your xibo instance. To do that please navigate to the applications page in your xibo instance, 
click the "Add application" button and give your application a name, such as *ApoXo*.

1) Copy 'ClientID' and 'Client Secret' and use them as properties in your 'apoxo.properties' file. 
2) Mark the check box 'Client Credentials? Allow the client Credentials Grant for this Client?'
3) Mark the check box 'All access' in the permission tab.

See [the following tutorial for a more detailed manual.](https://community.xibo.org.uk/t/1-8-api-introduction/7702)

#### Create and Specify the dataset
```aidl
xibo.datasetID=42
```
1) Navigate to the xibo dataset settings (baseurl/dataset/view) and create a new data set. 
2) You will find the data set ID before the name of your data set in the data set overview.
   Add it to your 'apoxo.properties' file.
3) Add columns that you wish to import from your csv sheets (e.g. Apothekenname, PLZ, Strasse,... ) <br>
   **Remark:** Only columsn which are part of your xibo data set will be imported.
   
The dataset will always be overwritten if **ApoXo** is executed.

## Developer information
### Building
*I am coming soon*
### Testing
*I am coming soon*