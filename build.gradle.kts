import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.4.10"
    application
    // gradle plugin which writes additional data in manifest
    id("com.coditory.manifest") version "0.1.9"
    // plugin to enable gradle release tasks
    id ("nebula.release") version "15.2.0"
    id("org.jlleitschuh.gradle.ktlint") version "10.0.0"
    id("org.jlleitschuh.gradle.ktlint-idea") version "10.0.0"
}

group = "me.daniel"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation(kotlin("test-junit"))
    implementation("com.squareup.okhttp3:okhttp:4.9.0")
    implementation ("com.squareup.retrofit2:retrofit:2.5.0")
    implementation ("com.squareup.retrofit2:converter-scalars:2.5.0")
    implementation ("com.squareup.retrofit2:converter-gson:2.4.0")
}
tasks.withType<KotlinCompile>() {
    kotlinOptions.jvmTarget = "1.8"
}
application {
    mainClassName = "MainKt"
}

tasks.jar {
    manifest {
        attributes["Main-Class"] = "MainKt"
        attributes["Implementation-Title"]="ApoXo"
    }
    configurations["compileClasspath"].forEach { file: File ->
        from(zipTree(file.absoluteFile))
    }
}

tasks.register<Copy>("copyLuckyCloud") {
    from(file("$buildDir/distributions"))
    include("*.tar")
    into(file("/home/daniel/LuckyCloud/GitLab/Releases"))
}

// just for learning purpose. define and regiser an own task:
abstract class GreetingTask : DefaultTask() {
    @TaskAction
    fun greet() {
        println("hello from GreetingTask")
    }
}
// Use the default greeting
tasks.register<GreetingTask>("hello")